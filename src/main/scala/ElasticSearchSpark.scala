import org.apache.spark.sql.SparkSession
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions.{col, date_format, unix_timestamp}
import org.apache.spark.sql.types.{DataTypes, TimestampType}

import scala.reflect.macros.whitebox
object ElasticSearchSpark {
  def writeDataToES(sparkSession: SparkSession,pathInput:String): Unit ={
    val df = sparkSession.read.parquet(pathInput)
    import sparkSession.implicits._
    import org.elasticsearch.spark.sql._
    val dfWithNewDT = df.withColumn("Timestamp",date_format(unix_timestamp(col("Timestamp"), "yyyyMMddHHmmss").cast("timestamp"), "yyyy-MM-dd HH-mm-ss"))
    dfWithNewDT.saveToEs("estest")
  }
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    try{
      val spark = SparkSession.builder()
        .config("spark.es.nodes","127.0.0.1")
        .config("spark.es.nodes.wan.only","true")
        .config("spark.es.index.auto.create", "true")
        .config("spark.es.port","9200")
        .appName("ES")
        .master("local[*]")
        .getOrCreate()
      val pathInput = "E:\\Tài liệu\\Big Data\\DataBigdata\\filesave=2020122513"
      writeDataToES(spark,pathInput)
    }
    catch{
      case e:Exception=>{
        println(e.getCause.getMessage+"|"+e.getCause.toString)
      }
    }

  }
}
